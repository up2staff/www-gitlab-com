<!-- This template is for Pick Your Brain blog posts – see https://about.gitlab.com/handbook/ea/#pick-your-brain-meetings -->

<!-- See https://about.gitlab.com/handbook/marketing/blog#pick-your-brain-posts for details on the blog process for PYB posts -->

### Summary

- **Date of upcoming PYB interview/livestream**:
- **Participants and contact details**:
- **Proposed discussion topics**:

### Checklist

- [ ] If [time sensitive](https://about.gitlab.com/handbook/marketing/blog/#process-for-time-sensitive-posts)
  - [ ] Added desired publish date as due date
  - [ ] Added ~"priority" label

/label ~"blog post"

/assign @skassabian

cc @rebecca
