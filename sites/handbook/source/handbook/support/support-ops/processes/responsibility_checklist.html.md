---
layout: handbook-page-toc
title: Support Operations Responsibility Checklist
description: General routine checklist of the responsibilities for Support Operations team. 
---

# Support Operations Responsibility Checklist

This is a general checklist of the responsibilities you will routinely.

* Daily
  * [ ] [Go Through Suspended/Deleted tickets](zendesk.html#suspended-and-deleted-tickets)
  * [ ] Check your email
  * [ ] [Triage the issues/MRs in support-ops-project](gitlab.html#triage)
  * [ ] Followup on existing issues/MRs
* Weekly
  * [ ] [Go through Feedback responses](index.html#reviewing-feedback)
  * [ ] [Weekly sync on issues/MRs with the Support-Ops team](index.html#weekly-sync)
* Monthly
  * [ ] [Schedule/Shadow 3 Support Engineers](index.html#support-shadowing)
  * [ ] [Monthly sync with Support-Ops team to discuss Feedback responses](index.html#monthly-sync)
* Quarterly
  * [ ] Send quarterly feedback survery (Support Ops Manager only)
* Every 6 months
  * [ ] Zendesk Role/User Audit (Support Ops Manager only)
