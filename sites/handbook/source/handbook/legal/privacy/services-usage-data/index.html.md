---
layout: handbook-page-toc
title: "Services Usage Data"
description: "This page ..."
---

#### Services Usage Data
GitLab collects information in order to help us understand how you use our products and services, so that we can improve and build better products and services.  For information on what is collected and your options, please see below. 
 
#### Self-managed GitLab software
GitLab collects information about usage from each Self-managed GitLab instance (Community Edition and Enterprise Edition) through a usage ping.  The usage ping sends a payload containing data such as total number of projects and pipelines, as well as license information and hostname to GitLab.  Only aggregates of usage data is sent:  no project names or other content is sent to GitLab.  You can view the exact payload of the usage ping in the administration panel in GitLab.  [Here](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html#usage-ping) describes how you can also opt-out of the usage ping.



