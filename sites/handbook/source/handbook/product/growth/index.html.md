---
layout: markdown_page
title: How the growth section works
---

## On this page

{:.no_toc}

- TOC
{:toc}

## Overview

The [Growth stage](/handbook/product/categories/#growth-stage) is responsible for
scaling GitLab's business value. To accomplish this, Growth analyzes the entire customer journey from acquisition of
a customer to the flow across multiple GitLab features - and even reactivation of lost users. Several groups
help with this mission:

- Activation, Conversion, Expansion, and Adoption connect users to the existing value that GitLab already delivers by
rapid experimentation.
- [Product Intelligence](https://about.gitlab.com/direction/product-intelligence/) builds the backbone of data that other groups need to be successful, enabling a data-informed product
culture at GitLab.

Growth's ultimate goal is to connect GitLab's value as a Single DevOps Platform with our customers. In order to do that, we take a zoom in and zoom out approach. We break down the entire GitLab growth model, and identify the highest ROI lever at this moment to focus on. In the [Growth direction page](https://about.gitlab.com/direction/growth/) we outline the Growth section's long term direction and near term focus areas.

## How the Growth section works

### Growth ideation and prioritization

Anyone is welcome to submit ideas to the [experiment idea backlog](https://gitlab.com/groups/gitlab-org/-/boards/2028884?&label_name[]=experiment%20idea&label_name[]=growth%20experiment) using the [experiment template](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/new?issuable_template=Growth%20experiment).

To prioritize, the growth groups will use the [ICE framework](https://blog.growthhackers.com/the-practical-advantage-of-the-ice-score-as-a-test-prioritization-framework-cdd5f0808d64), which consists of the following elements, scored on a scale of 1-10:

- Impact - high score for high impact
- Confidence - high score for high confidence
- Ease - high score for ease

### Weekly Growth meeting

The Growth Product Management Director should run a weekly growth meeting to review, prioritize, and plan experiments. The meeting should take place on Tuesdays for 50 min and should cover the following agenda.

5 min: Social questions

5 min: Announcements 

30 min: Group updates

- KPI & OKR (Live) 
- Highlights & Learnings (Live) 
- Activities (Read only)
- Next UP (Read only) 

10 min: Discussions


To prepare for the meeting, the Growth PM's and the Growth Director should check in on experiments to identify which can be concluded, and to collect data for review.

### Other regular Growth meetings

In addition to the weekly Growth meeting, Growth team members participate in the following regular meetings:

1. Weekly Growth Engineering
    1. Attendees: Growth team members
    1. Goals: Discuss anything Growth engineering related.
    1. Agenda: [link](https://drive.google.com/drive/search?q=type:document%20title:%22Growth%20Engineering%20Weekly%22)
1. Weekly Product Intelligence and Data Weekly Sync
    1. Attendees: Product Intelligence and Data
    1. Goals: Stay in-sync for work across Product Intelligence, Data Engineering, and Data Analysis.
    1. Agenda: [link](https://drive.google.com/drive/search?q=type:document%20title:%22Product%20Intelligence%20and%20Data%20Sync%22)
1. Weekly Growth & Threat Management EM
    1. Attendees: Growth & Threat Management EMs
    1. Goals: Stay in-sync on growth engineering efforts.
    1. Agenda: [link](https://drive.google.com/drive/search?q=type:document%20title:Growth%20%26%20Threat%20Management%20EM%20Staff%20Weekly%20Agenda)
1. Weekly Growth PM sync
    1. Attendees: Growth PMs
    1. Goals: Stay in sync as a PM team, discuss PM specific issues/ideas
    1. Agenda: [link](https://drive.google.com/drive/search?q=type:document%20title:%22Weekly%20Growth%20PM%20Team%20Call%22)
1. Product Intelligence, Fulfillment, Data, Customer Success, and Sales Weekly Sync
    1. Product Intelligence, Data, Customer Success, and Sales
    1. Goals: Stay in-sync for work across Product Intelligence, Data, Customer Success, and Sales
    1. Agenda: [link](https://drive.google.com/drive/search?q=type:document%20title:Product%20Intelligence,%20Data,%20Customer%20Success,%20and%20Sales%20Weekly%20Sync)
1. Monthly sync meeting with the Data team
    1. Attendees: Growth PMs and Data team
    1. Goals: Keep the Data team aware of upcoming changes that may impact them, get proactive feedback on how to set up data and tracking needs
    1. Agenda: [link](https://drive.google.com/drive/u/0/search?q=type:document%20title:%20Data:Growth%20PM%20Sync)
1. Monthly sync with Growth Marketing
    1. Attendees: Growth PMs and Growth Marketers
    1. Goals: Stay in-sync and work as efficiently as possible together
    1. Agenda: [link](https://drive.google.com/drive/u/0/search?q=type:document%20title:%20%22Growth%20//%20Marketing%20Sync%22)
1. Monthly sync with Product, Legal, Compliance, Data, and Privacy
    1. Attendees: Product, Legal, Compliance, Data, and Privacy
    1. Goals: Answer any legal, compliance, and privacy concerns
    1. Agenda: [link](https://drive.google.com/drive/u/0/search?q=type:document%20title:%20%22Data%20Governance%20Monthly%20Sync%22)
10. Monthly sync with Growth section and Application Security
    1. Attendees: Product, Engineering, Application security
    1. Goals: Growth / Application Security stable counterparts monthly discussion
    1. Agenda: [link](https://drive.google.com/drive/search?q=type:document%20title:%22Growth%20section%20and%20appsec%22)


### Growth Deliverables

A growth deliverable should be a released feature or a launched effort that is expected to directly impact KPIs and improve external or internal customer experiences.

We will use the label `~Growth-Deliverable` to differentiate this from a typical product deliverable. A "Growth-Deliverable" should be:

- Able to stand on its own (can be potentially released/launched on its own)
- Expected to directly impact KPI or improve internal/external user experience on its own
- Typically should be a product change, but can include emails etc. if it meet 1) AND 2)
- Growth deliverables can also be UX research or data analysis if these are large in scope and require a significant amount of effort from the Product Manager, OR if they provide valuable insight for growth experiments.

## How Growth launches experiments 

Since the Growth section is among the first groups to launch product experiments and A/B testing, we summarized the current tooling and process in the list of guides and documentation below to help other teams interested in experimentation to get started faster.  

- [Experiment guide for Product managers](https://gitlab.com/gitlab-org/growth/experimentation/-/issues/14)
- [Experiment ideation Process](https://gitlab.com/gitlab-org/growth/experiment-design-repo/-/issues/1)
- [Engineering guide for how to run experiment](https://docs.gitlab.com/ee/development/experiment_guide/)
- [Growth Engieering Handbook page on running experiments](https://about.gitlab.com/handbook/engineering/development/growth/#running-experiments)
- [A way for GitLab team members to view currently active experiments on GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/issues/262725)

## How Growth collaborates with other groups

The hypotheses explored by Growth will span across groups and stages. How do other groups interact with Growth?

Growth does not own areas of the product, but finds ways to help users discover and unlock value throughout the GitLab
application. They will do a combination of shipping low-hanging fruit themselves, but will partner with other stages
on larger initiatives that need specialized knowledge or ongoing focus.

It is the responsibility of other stages to maintain and expand core value in their respective product areas; it's
Growth's mission to streamline delivery of that value to users. For example:

- [Manage](/handbook/product/categories/#manage-stage) is responsible for user registration and
management. This stage maintains a long-term vision on how users are created and maintained in GitLab, with a roadmap that
includes other categories and priorities.
- Growth may identify user registration as an opportunity to further the group's [priorities](#growth-ideation-and-prioritization). Growth's
engineering team may ship smaller improvements independently of Manage's direct involvement during implementation (such as [soft email confirmation](https://gitlab.com/gitlab-org/growth/product/issues/7)), but may need to partner with Manage on larger efforts like a completely [new onboarding](https://gitlab.com/gitlab-org/gitlab-ce/issues/57832) experience.
- The group ultimately owning the change - in this case, Manage - would review Growth's contributions into their area of the product
and - like a contribution coming from the wider community - ultimately own the final result.

### Collaboration process with other Product teams
 

We use this [process](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/233) to make sure growth and product teams are maximizing business impact while ensuring close collaboration.

#### Proposed Growth-Feature Product Owner collaboration process

1. Growth identifies a focus area that may potentially touch the areas owned by another product team. For example, growth will be focusing on new user adoption of Create & Verify features starting in FY21 Q4.

1. Growth reaches out to have a sync or async kickoff conversation with the product team (ideally including PM, EM, UX). The goal of the kickoff is to:

   * Inform the Product feature owner that growth is planning to experiment in this area;
   * Share growth's high level plans and ideas to avoid conflicting roadmap and duplicate efforts;
   * Form a plan how the 2 teams can keep each other updated async, for example, ping the PMs on the relevant issues async or sync meetings etc.
   * Ideally, establish understanding of both teams' KPIs and objectives 

3. Growth will post regular updates on what we are working on, what has been shipped, what's up & next, and the results & learnings from analyzed experiments to channels such as key meetings, GC, and potentially a monthly summary video.

4. Most growth experiments will be focused on UX/copy flow changes initially, and these will follow the typical GitLab code review process.

5. We don't expect to have many new "features" developed from growth experiments, but in the rare case that this happens or if we feel like the UX flow we developed needs to have a permanent owner, the Growth team will work to identify an appropriate product owner, and hand it off to the owner following the typical community contribution process.


#### Proposed Growth-Feature Product Owner collaboration process

1. Growth identifies a focus area that may potentially touch the areas owned by another product team, for example, growth will be focusing on new user adoption of Create & Verify features starting in FY21 Q4

1. Growth reaches out to have a kickoff conversation with the product team (ideally including PM, EM, UX). The goal of the kickoff is to:

* Inform the Product feature owner that growth is planning to experiment in this area;
* Share growth's high level plans and ideas to avoid conflicting roadmap and duplicate efforts;
* Form a plan how the 2 teams can keep each other updated async, for example, ping the PMs on the relevant issues async or sync meetings etc.
* Ideally, establish understanding of both teams' KPIs and objectives 

3. Growth will post regular updates on what we are working on, what have been shipped, what's up & next, results & learnings from analyzed experiments via channels such as key meetings, GC and potentially a monthly summary video

4. Most growth experiments will be focused on UX/copy flow changes initially, and these will follow they typical GitLab code review process

5. We don't expect to have many new "features" developed from growth experiments, but in rare case if this happens, or we feel the UX flow we developed need to have a permanent owner, we will hand it off to the relevant product owner, following the typical community contribution process.


### Growth RADCIE and DRIs

In alignment with Gitlab's [RADCIE](/handbook/people-group/directly-responsible-individuals/#radcie) approach, the DRIs for the following tasks would be:

| Task | DRI |
| ---- | --- |
| Hypothesis Generation | Growth Product/Engineering |
| Experiment Design | Growth Product/Engineering |
| Experiment Implementation | Growth Product/Engineering |
| Contribution Review | Stage Product/Engineering |
| Experiment Ramp | Growth Product/Engineering |
| Post-Experiment Analysis | Data Team |
| Post-Experiment Decision | Growth Product/Engineering |
| Maintenance | Stage Product/Engineering |
| Alert creation | Growth Product/Engineering : [How to create a Sisense SQL alert](/handbook/engineering/development/growth/sisense_alert.html) |


