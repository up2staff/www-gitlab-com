---
layout: markdown_page
title: "Portfolio Management"
description: "As multiple projects scale the organization needs to adopt processes to manage and govern the portfolio of agile projects. Learn more here!"
canonical_path: "/solutions/portfolio-management/"
---
## Agile Portfolio Management
As multiple projects scale, with parallel value streams and efforts, the organization needs to adopt processes to manage and govern the portfolio of agile projects, both in flight and proposed.  

## Plan future work with Epics

| <br> ● Organize new business initiatives and efforts into [epics](https://docs.gitlab.com/ee/user/group/epics/) <br> <br> ● Plan sub epics and issues into sprints and milestones  <br> <br> ● Plan and define strategic plans | ![Epics](https://docs.gitlab.com/ee/user/group/epics/img/epic_view_v13.0.png) |

## Roadmaps visualize value delivery

| <br> ● Prioritize and visualize sequence of delivery with [roadmaps](https://docs.gitlab.com/ee/user/group/roadmap/) <br> <br> ● Communicate plans, timing, and strategic flow  <br> <br> ● Maintain visibility from strategic plans to execution | ![Roadmaps](https://docs.gitlab.com/ee/user/group/roadmap/img/roadmap_view_v13_2.png) |
